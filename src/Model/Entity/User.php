<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{
    // https://book.cakephp.org/3.0/en/tutorials-and-examples/cms/authentication.html#adding-password-hashing
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }

    // https://book.cakephp.org/3.0/en/orm/entities.html#creating-virtual-properties
    protected function _getFullName()
    {
        return $this->first_name . '  ' . $this->last_name;
    }
}
