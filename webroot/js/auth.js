// Initialize Firebase
var config = {
    apiKey: "AIzaSyBLwX8VAiBx-H6u1LY2HnqHW23js18WF7E",
    authDomain: "sendit-2277a.firebaseapp.com",
    databaseURL: "https://sendit-2277a.firebaseio.com",
    projectId: "sendit-2277a",
    storageBucket: "sendit-2277a.appspot.com",
    messagingSenderId: "848006971678"
};
firebase.initializeApp(config);
var provider = new firebase.auth.FacebookAuthProvider();

var btnFacebook = document.getElementById('btn-facebook');
btnFacebook.addEventListener('click', function () {
    firebase.auth().signInWithPopup(provider).then(function (result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;

        var csrfToken = document.getElementsByName('_csrfToken')[0].value;

        axios.post(SENDIT_FACEBOOK_ENDPOINT, {
            email: user.email,
            name: user.displayName
        }, { headers: { 'X-CSRF-Token': csrfToken } })
            .then(function (response) {
                console.log('Success', SENDIT_BASE_URL);
                window.location.href = SENDIT_BASE_URL;
            })
            .catch(function (error) {
                alert('Something bad happened...');
            });
    }).catch(function (error) {
        alert('Something bad happened...');
    });
});
