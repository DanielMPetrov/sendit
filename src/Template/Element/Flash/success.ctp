<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success py-3" role="alert">
    <i class="far fa-check-circle"></i> <?= $message ?>
</div>
