<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use App\Model\Entity\Reply;
use Cake\Log\Log;

class RepliesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index']);
        $this->loadComponent('RequestHandler');
    }

    public function isAuthorized($user)
    {
        $action = $this->request->action;

        // Users can add reply if either of the following is true:
        // the message is global, it is in their inbox, they are the author, or they are admin
        if ($action == 'add') {
            $message_id = $this->request->data('message_id');

            if (TableRegistry::get('Messages')->isOwnedBy($message_id, $user['id'])) {
                return true;
            }

            if (TableRegistry::get('Inbox')->isGlobal($message_id)) {
                return true;
            }

            if (TableRegistry::get('Inbox')->isInUserInbox($message_id, $user['id'])) {
                return true;
            }
        }

        if ($action == 'delete') {
            return true;
        }

        return parent::isAuthorized($user);
    }

    private function logReplySuccess(Reply $reply)
    {
        Log::info("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] sent reply #{$reply->id} to message #{$reply->message_id}", 'replies');
    }

    private function logReplyError(Reply $reply)
    {
        Log::error("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] failed sending a reply to message #{$reply->message_id}", 'replies');
    }

    public function add()
    {
        $this->request->allowMethod('post');

        $reply = TableRegistry::get('Replies')->newEntity($this->request->getData());
        $reply->user_id = $this->Auth->user('id');

        if (TableRegistry::get('Replies')->save($reply)) {
            $this->logReplySuccess($reply);
            $this->Flash->success('Reply sent successfully.');
        } else {
            $this->logReplyError($reply);
            $this->Flash->error('Reply failed to send. Please try again later.');
        }

        return $this->redirect($this->referer());
    }

    // https://book.cakephp.org/3.0/en/tutorials-and-examples/blog/part-two.html#deleting-articles
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $reply = $this->Replies->get($id);

        // if it's not the owner and is not an admin, it's a bad actor
        if ($reply->user_id != $this->Auth->user('id') && $this->Auth->user('role') != 'admin') {
            throw new BadRequestException;
        }

        if ($this->Replies->delete($reply)) {
            $this->Flash->success('This comment has been deleted.');
        } else {
            $this->Flash->error('Failed deleting comment. Please try again later.');
        }

        return $this->redirect($this->referer());
    }

    // API endpoints

    /**
     * Get message replies for a specific message (HTTP GET)
     * 
     * url: api/messages/123/replies.json
     */
    public function index()
    {
        $message = TableRegistry::get('Messages')->get(
            $this->request->getParam('message_id'), [
                'contain' => ['replies']
            ]
        );

        $this->set([
            'replies' => $message->replies,
            '_serialize' => ['replies']
        ]);
    }
}
