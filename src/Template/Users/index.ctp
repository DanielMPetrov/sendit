<?php $this->assign('title', 'Users'); ?>

<div class="row">
    <div class="col-md-10 offset-md-1">
        <?= $this->Flash->render() ?>
        <h4 class="mt-3 mb-4 display-4" style="font-size:2rem;">Users</h4>
        <div class="row">
            <?php foreach ($users as $user) : ?>
            <div class="col-lg-4 col-sm-6">
                <div class="card mb-4 shadow-sm">
                    <div class="card-body">
                        <?php if ($isAdmin) : ?>
                        <h5 class="card-title">
                            <a href="<?= $this->Url->build("/users/messages/$user->id") ?>" class="card-link">
                                <?= $user->full_name ?>
                            </a>
                            <?php if ($user->role === 'admin') : ?>
                            <span class="badge badge-danger" style="font-size:.8rem;">admin</span>
                            <?php endif; ?>
                        </h5>
                        <?php else: ?>
                        <h5 class="card-title">
                            <?= $user->full_name ?>
                            <?php if ($user->role === 'admin') : ?>
                            <span class="badge badge-danger" style="font-size:.8rem;">admin</span>
                            <?php endif; ?>
                        </h5>
                        <?php endif; ?>
                        <h6 class="card-subtitle mb-2 text-muted"><?= $user->email ?></h6>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
