<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger py-3" role="alert">
    <i class="fas fa-exclamation-circle"></i> <?= $message ?>
</div>
