<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Message;

class InboxTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Users');
        $this->belongsTo('Messages');
    }

    public function isGlobal($messageId)
    {
        return $this->exists(['message_id' => $messageId, 'user_id IS NULL']);
    }

    public function isInUserInbox($messageId, $userId)
    {
        return $this->exists(['message_id' => $messageId, 'user_id' => $userId]);
    }

    public function sendToAll(Message $message)
    {
        $inbox = $this->newEntity();
        $inbox->message = $message;
        return $this->save($inbox);
    }

    public function sendToEmails(array $emails, Message $message)
    {
        $usersTable = TableRegistry::get('Users');

        // get all users by email
        $users = $usersTable->find()
            ->where(['email IN' => $emails])
            ->toArray();

        // add the message in each of their inbox
        foreach ($users as $user) {
            $inbox = $this->newEntity();
            $inbox->message = $message;
            $user->inbox = [$inbox];
        }

        return $usersTable->saveMany($users);
    }
}
