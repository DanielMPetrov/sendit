<?php $this->assign('title', h($message->subject)); ?>
<?php $author = $message->user ?>
<?php $image = $message->attachment_path ?>

<div class="row">
    <div class="col-md-8 offset-md-2">
        <?= $this->Flash->render() ?>

        <div class="card px-5 py-3">
            <div class="card-body" >
                <h2 class="display-4" style="font-size:2rem;"><?= h($message->subject) ?></h2>
                <p class="text-muted" style="margin-top: -8px; font-size: 14px">
                    by <strong>
                        <?= isset($author) ? $author->full_name : 'anonymous' ?>
                        <?php if ($author->role === 'admin') : ?>
                        <span class="badge badge-danger" style="font-size:.8rem;">admin</span>
                        <?php endif; ?>
                    </strong>
                    on <?= $message->display_date_sent ?>
                </p>
                <p><?= h($message->content) ?></p>
                <?php if ($image): ?>
                <div class="px-4">
                    <img
                        src="<?= $this->Url->build("/img/$image") ?>"
                        class="img-fluid mb-3"
                    />
                </div>
                <?php endif; ?>
                <?php if ($user['role'] == 'admin') : ?>
                <div class="text-right">
                    <button class="btn btn__delete-reply--light" onclick="showDeleteAlert(this);">Delete</button>
                    <?= $this->Form->create(null, [
                        'url' => ['controller' => 'Messages', 'action' => 'delete', $message->id],
                        'style' => 'display: none;'
                    ]) ?>
                    <?= $this->Form->end() ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="row my-3">
    <div class="col-md-8 offset-md-2">
        <h5 class="my-3">
            Post a reply 
            <small class="text-muted">as <?= $user['first_name'] ?> <?= $user['last_name'] ?>
                <?php if ($user['role'] === 'admin') : ?>
                <span class="badge badge-danger" style="font-size:.8rem;">admin</span>
                <?php endif; ?>
            </small>
        </h5>

        <?= $this->Form->create(null, [
            'url' => ['controller' => 'Replies', 'action' => 'Add']
        ]) ?>
            <input name="message_id" type="hidden" value="<?= $message->id ?>">
            <div class="form-group">
                <textarea name="content" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-outline-primary">Reply</button>
            </div>
        <?= $this->Form->end() ?>

        <h5 id="discussion">Discussion</h5>
        <?php if (empty($message->replies)) : ?>
        <p class="text-muted mt-4">
            <i class="far fa-lightbulb text-warning"></i> Replies to this message will appear here.
        </p>
        <?php endif; ?>

        <?php foreach ($message->replies as $reply) : ?>
        <div class="py-5" style="border-bottom: 2px solid hsla(212, 56%, 16%, 0.2);">
            <h6 class="card-title mb-0"><?= $reply->user->full_name ?>
                <?php if ($reply->user['role'] === 'admin') : ?>
                <span class="badge badge-danger" style="font-size:.8rem;">admin</span>
                <?php endif; ?>
            </h6>
            <small class="text-muted"><?= $reply->created->format('F j, Y \a\t g:i a') ?></small>
            <p class="card-text mt-2"><?= h($reply->content) ?></p>
            <?php if ($reply->user_id == $user['id'] || $user['role'] == 'admin') : ?>
            <div class="text-right">
                <button class="btn btn__delete-reply" onclick="showDeleteAlert(this);">Delete</button>
                <?= $this->Form->create(null, [
                    'url' => ['controller' => 'Replies', 'action' => 'delete', $reply->id],
                    'style' => 'display: none;'
                ]) ?>
                <?= $this->Form->end() ?>
            </div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<?php $this->start('script'); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // https://sweetalert.js.org/guides/
    function showDeleteAlert(button) {
        swal({
            title: 'Are you sure?',
            text: 'Once deleted, there is no undo!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                button.nextElementSibling.submit();
            } else {
                swal("Delete cancelled!", {
                    icon: "error"
                });
            }
        });
    }
</script>
<?php $this->end(); ?>
