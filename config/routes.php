<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Messages', 'action' => 'home']);

    // https://book.cakephp.org/3.0/en/development/routing.html#route-elements
    $routes->connect(
        ':id/message/:day/:month/:year/:slug',
        ['controller' => 'Messages', 'action' => 'details']
    )
        ->setPass(['id', 'day', 'month', 'year', 'slug'])
        ->setPatterns([
            'year' => '[12][0-9]{3}',
            'month' => '0[1-9]|1[012]',
            'day' => '0[1-9]|[12][0-9]|3[01]'
        ]);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/api', function (RouteBuilder $routes) {
    $routes->setExtensions(['json', 'xml']);

    $routes->resources('Users', function (RouteBuilder $routes) {
        $routes->resources('Messages');
    });

    $routes->resources('Messages', function (RouteBuilder $routes) {
        $routes->resources('Replies');
    });

    $routes->resources('MessagesApi');
});
