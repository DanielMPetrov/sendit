<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;

class MessagesApiController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'delete']);
        $this->loadComponent('RequestHandler');
    }

    /**
     * Creates a new message (HTTP POST)
     * 
     * url: /api/messages_api.json
     */
    public function add()
    {
        $userId = $this->request->getData('user_id');
        if ($userId) {
            $userExists = TableRegistry::get('users')->exists(['id' => $userId]);
            if (!$userExists) {
                $this->response->statusCode(400);
                $this->set([
                    'error' => 'The specified user_id does not exist',
                    '_serialize' => ['error']
                ]);
                return;
            }
        }

        $messages = TableRegistry::get('Messages');
        $message = $messages->newEntity($this->request->getData());

        if ($message->hasErrors()) {
            $this->response->statusCode(400);
            $this->set([
                'errors' => $message->getErrors(),
                '_serialize' => ['errors']
            ]);
            return;
        }

        $to = trim($this->request->getData('to'));

        if ($to === 'all') {
            if (!TableRegistry::get('Inbox')->sendToAll($message)) {
                return $this->response->withStatus(500);
            }
        } else {
            if (!TableRegistry::get('Inbox')->sendToEmails($this->getToArray(), $message)) {
                return $this->response->withStatus(500);
            }
        }

        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }

    /**
     * Delete a message by id (HTTP DELETE)
     * 
     * url: /api/messages_api/42
     */
    public function delete($id)
    {
        $messages = TableRegistry::get('Messages');

        $message = $messages->find()->where(['id' => $id])->first();

        if (!$message) {
            return $this->response->withStatus(404);
        }

        if ($message->attachment_path) {
            $file = new File(WWW_ROOT . 'img' . DS . $message->attachment_path);
            $file->delete();
        }

        if (!$messages->delete($message)) {
            return $this->response->withStatus(500);
        }

        return $this->response->withStatus(204);
    }

    private function getToArray()
    {
        $to = trim($this->request->data('to'));
        $to = trim($to, ',');
        return array_map('trim', explode(',', $to));
    }
}
