<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

class AuthController extends AppController
{
    private function logUserSuccess($user)
    {
        Log::info("User #{$user['id']} {$user['email']} [{$this->request->clientIp()}] logged in with {$user['role']} privileges.", 'auth');
    }

    private function logUserError(string $email)
    {
        Log::warning("User $email [{$this->request->clientIp()}] failed to login.", 'auth');
    }

    // https://book.cakephp.org/3.0/en/tutorials-and-examples/cms/authentication.html#adding-login
    public function login()
    {
        // redirect authenticated users away from login
        if ($this->Auth->user()) {
            return $this->redirect('/');
        }

        if ($this->request->isPost()) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->logUserSuccess($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->logUserError($this->request->getData('email'));
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'join', 'facebook']);
    }

    public function facebook()
    {
        $this->request->allowMethod('post');

        // Validate and convert to an Entity object
        $usersTable = TableRegistry::get('Users');

        // check if the user is already in our database
        $entity = $usersTable->find()
            ->where(['email' => $this->request->data('email')])
            ->first();

        if (!$entity) {
            $entity = $usersTable->newEntity([
                'first_name' => $this->request->data('name'),
                'email' => $this->request->data('email'),
                'role' => 'user'
            ]);

            if (!$usersTable->save($entity)) {
                return $this->response->withStatus(400);
            }
        }

        // log in 
        $this->Auth->setUser($entity);
        $this->logUserSuccess($entity);

        return $this->response->withStatus(200);
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function join()
    {
        // redirect authenticated users away from register
        if ($this->Auth->user()) {
            return $this->redirect('/');
        }

        if ($this->request->isPost()) {
            $this->addUser();
        }
    }

    private function addUser()
    {
        // Validate and convert to an Entity object
        $usersTable = TableRegistry::get('Users');
        $entity = $usersTable->newEntity($this->request->data());
        $entity->role = 'user';

        if (!$usersTable->save($entity)) {
            $this->Flash->error('Something went wrong. Please try again later.');
            return $this->redirect(['action' => 'join']);
        }

        $this->Flash->success("Welcome aboard, $entity->first_name. You can now login.");
        return $this->redirect(['action' => 'login']);
    }
}
