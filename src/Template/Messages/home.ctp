<?php $this->assign('title', 'Home'); ?>

<div class="row">
    <div class="col-md-8 offset-md-2">
        <?= $this->Flash->render() ?>

        <h4 class="mt-3 mb-4 display-4" style="font-size: 2rem;">
            Send message
            <small class="text-muted">as <?= $user['email'] ?? 'anonymous' ?>
                <?php if ($user['role'] === 'admin') : ?>
                <span class="badge badge-danger" style="font-size:.9rem;">admin</span>
                <?php endif; ?>
            </small>
        </h4>
        <?= $this->Form->create($message, ['type' => 'file', 'novalidate' => true]) ?>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">To</div>
                    </div>
                    <?php if ($user) : ?>
                    <?= $this->Form->text('to', [
                        'class' => 'form-control',
                        'placeholder' => 'Recipients'
                    ]) ?>
                    <?php else : ?>
                    <?= $this->Form->text('to', [
                        'class' => 'form-control',
                        'placeholder' => 'Recipients',
                        'readonly',
                        'value' => 'all'
                    ]) ?>
                    <?php endif; ?>
                </div>
                <?php if ($user) : ?>
                <small class="form-text text-muted">
                    Enter a comma separated list of emails, or 'all' to target everyone.
                </small>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <?= $this->Form->text('subject', [
                    'class' => 'form-control',
                    'placeholder' => 'Add a subject'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->textarea('content', [
                    'class' => 'form-control',
                    'rows' => 7,
                    'placeholder' => 'Add a message'
                ]) ?>
            </div>
            <div class="form-group">
                <label for="image">Image Attachment (optional)</label>
                <?= $this->Form->file('image', [
                    'class' => 'form-control-file'
                ]); ?>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-outline-primary">Send Message</button>
            </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="row my-2">
    <div class="col-md-8 offset-md-2">
        <h4 id="psa" class="display-4" style="font-size: 1.75rem;">Public Announcements</h4>

        <?php if (empty($global)) : ?>
        <p class="text-muted mt-4">
            <i class="far fa-lightbulb text-warning"></i> Public announcements will appear here.
        </p>
        <?php endif; ?>

        <?php foreach ($global as $inbox) : ?>
        <?php $message = $inbox->message ?>
        <?php $author = $message->user ?>
        <div class="card my-4">
            <div class="card-body p-4">
                <h4 class="card-title mb-0">
                    <?= h($message->subject) ?>
                </h4>
                <small class="text-muted">
                    by <strong><?= isset($author)
                        ? ($user['role'] === 'admin'
                            ? "<a href='{$this->Url->build("/users/messages/$author->id")}' class='card-link'>$author->full_name</a>"
                            : $author->full_name)
                        : 'anonymous' ?></strong>
                    <?php if ($author->role === 'admin') : ?>
                    <span class="badge badge-danger" style="font-size:.7rem;">admin</span>
                    <?php endif; ?>
                    on <?= $message->display_date_sent ?>
                </small>
                <p class="card-text mt-3"><?= h($message->content) ?></p>
            </div>
            <div class="card__footer p-3">
                <a href="<?= $this->Url->build($message->permalink) ?>" class="btn">
                    <i class="fas fa-envelope-open"></i> View Message
                </a>
                <a href="<?= $this->Url->build($message->permalink) ?>#discussion" class="btn">
                    <i class="fas fa-comments"></i>
                    <?= $message->reply_count == 0
                        ? 'Reply'
                        : ($message->reply_count == 1
                            ? "1 Reply"
                            : "$message->reply_count Replies") ?>
                </a>
                <?php if ($user['role'] == 'admin') : ?>
                <button class="btn card-link btn-link" onclick="showDeleteAlert(this);">
                    <i class="fas fa-trash"></i> Delete
                </button>
                <?= $this->Form->create(null, [
                    'url' => ['controller' => 'Messages', 'action' => 'delete', $message->id],
                    'style' => 'display: none;'
                ]) ?>
                <?= $this->Form->end() ?>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>

<?php $this->start('script'); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // https://sweetalert.js.org/guides/
    function showDeleteAlert(button) {
        swal({
            title: 'Are you sure?',
            text: 'Once deleted, there is no undo!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                button.nextElementSibling.submit();
            } else {
                swal("Delete cancelled!", {
                    icon: "error"
                });
            }
        });
    }
</script>
<?php $this->end(); ?>
