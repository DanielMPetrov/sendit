<?php $user = $this->Session->read('Auth.User'); ?>
<?php $action = $this->request->action; ?>

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->fetch('title') ?> · SendIT</title>
    <?= $this->Html->meta('icon') ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->css('layout') ?>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light navigation">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?= $this->Url->build('/') ?>">SendIT</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <div class="navbar-nav ml-auto">
                    <?php if ($user) : ?>
                    <ul class="navbar-nav navigation__list">
                        <li class="nav-item <?= $action == 'home' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= $this->Url->build('/') ?>">
                                <i class="fas fa-home"></i> Home
                            </a>
                        </li>
                        <li class="nav-item <?= $action == 'inbox' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= $this->Url->build('/messages/inbox') ?>">
                                <i class="fas fa-envelope"></i> Inbox
                            </a>
                        </li>
                        <li class="nav-item <?= $action == 'sent' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= $this->Url->build('/messages/sent') ?>">
                                <i class="fas fa-paper-plane"></i> Sent
                            </a>
                        </li>
                        <li class="nav-item <?= $action == 'index' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= $this->Url->build('/users') ?>">
                                <i class="fas fa-users"></i> Users
                            </a>
                        </li>
                    </ul>
                    <?= $this->Form->create(null, [
                        'url' => ['controller' => 'Auth', 'action' => 'logout'],
                        'class' => 'form-inline'
                    ]) ?>
                        <button class="btn btn-outline-dark btn-sm my-2 my-sm-0" type="submit">
                            Sign out
                        </button>
                    <?= $this->Form->end() ?>
                    <?php else : ?>
                    <a href="<?= $this->Url->build(["controller" => "Auth", "action" => "login"]) ?>"
                        class="nav-link">Sign in</a>
                    <a href="<?= $this->Url->build(["controller" => "Auth", "action" => "join"]) ?>"
                        class="btn btn-outline-primary">Sign up</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </nav>

    <div class="container">
        <?= $this->fetch('content') ?>
    </div>

    <footer class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center text-md-left">
                    &copy; <?= date('Y'); ?> SendIT by Daniel Petrov
                </div>
                <div class="col-sm-6 text-center text-md-right mt-3 mt-sm-0">
                    <a href="#" class="nav-link p-0">Back to top</a>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <?= $this->fetch('script') ?>
</body>
</html>
