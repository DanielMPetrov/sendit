<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MessagesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users');
        $this->hasMany('Inbox', ['dependent' => true]);
        $this->hasMany('Replies', ['dependent' => true]);
    }

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('to')
            ->notEmpty('to')
            ->requirePresence('subject')
            ->notEmpty('subject', 'Subject cannot be left empty')
            ->maxLength('subject', 100, 'Subject cannot be longer than 100 characters')
            ->requirePresence('content')
            ->notEmpty('content', 'Message cannot be left empty')
            ->minLength('content', 20, 'Your message must be at least 20 characters')
            ->maxLength('content', 2000, 'Your message cannot be longer than 2000 characters');
    }

    // https://book.cakephp.org/3.0/en/tutorials-and-examples/blog-auth-example/auth.html
    public function isOwnedBy($messageId, $userId)
    {
        return $this->exists(['id' => $messageId, 'user_id' => $userId]);
    }
}
