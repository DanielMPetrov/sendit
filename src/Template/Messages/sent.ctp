<?php $this->assign('title', 'Sent'); ?>

<div class="row my-2">
    <div class="col-md-8 offset-md-2">
        <?= $this->Flash->render() ?>
        <h4 class="mt-3 mb-4 display-4" style="font-size:2rem;">Sent</h4>

        <?php if (empty($sent)) : ?>
        <p class="text-muted">
            <i class="far fa-lightbulb text-warning"></i> Messages sent by you will appear here.
        </p>
        <?php endif; ?>

        <?php foreach ($sent as $message) : ?>
        <div class="card my-4">
            <div class="card-body">
                <h5 class="card-title mb-0">
                    <a href="<?= $this->Url->build($message->permalink) ?>" class="card-link">
                        <?= h($message->subject) ?>
                    </a>
                </h5>
                <small class="text-muted">
                    to <strong><?= $message->to_display_list ?></strong>
                    on <?= $message->display_date_sent ?>
                </small>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>
