<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;
use App\Model\Entity\Message;
use Cake\Http\Exception\BadRequestException;
use Cake\Log\Log;
use Cake\Filesystem\File;

class MessagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'home', 'index']);
        $this->loadComponent('RequestHandler');
    }

    // https://book.cakephp.org/3.0/en/tutorials-and-examples/blog-auth-example/auth.html
    public function isAuthorized($user)
    {
        $action = $this->request->action;

        // All registered users can view inbox and sent messages
        if (in_array($action, ['inbox', 'sent'])) {
            return true;
        }

        if ($action == 'details') {
            $message = TableRegistry::get('Messages')->get($this->request->id);

            if ($message->user_id == $this->Auth->user('id')) {
                return true;
            }

            if (TableRegistry::get('Inbox')->isGlobal($message->id)) {
                return true;
            }

            if (TableRegistry::get('Inbox')->isInUserInbox($message->id, $user['id'])) {
                return true;
            }

            if (!parent::isAuthorized($user)) {
                $this->logMessageAccessError($message);
                return false;
            }

            return true;
        }

        return parent::isAuthorized($user);
    }

    private function getToArray()
    {
        $to = trim($this->request->data('to'));
        $to = trim($to, ',');
        return array_map('trim', explode(',', $to));
    }

    private function logMessageSuccess(Message $message)
    {
        if ($this->Auth->user()) {
            Log::info("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] sent message #{$message->id}", 'messages');
        } else {
            Log::info("Anonymous user [{$this->request->clientIp()}] sent message #{$message->id}", 'messages');
        }
    }

    private function logMessageError(string $to)
    {
        if ($this->Auth->user()) {
            Log::error("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] failed to send a message addressed to: $to", 'messages');
        } else {
            Log::error("Anonymous user [{$this->request->clientIp()}] failed to send message addressed to: $to", 'messages');
        }
    }

    private function add(Message $message)
    {
        $this->Messages->patchEntity($message, $this->request->getData());

        if ($message->hasErrors()) {
            $hint = 'Please amend your form and try again.';
            $hint .= '<ul class="mb-0">';
            foreach ($message->geterrors() as $key => $errorArray) {
                $e = reset($errorArray);
                $hint .= "<li>$e</li>";
            }
            $hint .= '</ul>';
            $this->Flash->error($hint, ['escape' => false]);
            return;
        }

        $message->user_id = $this->Auth->user('id');

        $to = trim($this->request->getData('to'));

        // if the user is anonymous or 'to' is set to 'all'
        if (!$this->Auth->user() || $to === 'all') {
            if (!TableRegistry::get('Inbox')->sendToAll($message)) {
                $this->logMessageError($to);
                $this->Flash->error('Message failed to send. Please try again later.');
                return;
            }
        }
        // if user is authenticated and to is not set to all
        else {
            if (!TableRegistry::get('Inbox')->sendToEmails($this->getToArray(), $message)) {
                $this->logMessageError($to);
                $this->Flash->error('Message failed to send. Please try again later.');
                return;
            }
        }

        $imageName = $this->saveImage($this->request->getData('image'), $message->id);
        if ($imageName) {
            $message->attachment_path = $imageName;
            $this->Messages->save($message);
        }

        $this->logMessageSuccess($message);
        $this->Flash->success('Message sent successfully.');
        return $this->redirect(['action' => 'home']);
    }

    private function saveImage(array $image, int $messageId)
    {
        $src = $image['tmp_name'];
        if (!$src) {
            return false;
        }
        
        $newName = $messageId . '_' . $image['name'];
        $dest = WWW_ROOT . 'img' . DS . $newName;
        $moved = move_uploaded_file($src, $dest);
        if (!$moved) {
            return false;
        }

        return $newName;
    }

    public function home()
    {
        $message = $this->Messages->newEntity();

        if ($this->request->isPost()) {
            $this->add($message);
        }

        $global = TableRegistry::get('Inbox')->find()
            ->where(function ($exp, $q) {
                return $exp->isNull('Inbox.user_id');
            })
            ->contain(['Messages.Users'])
            ->orderDesc('Messages.created')
            ->toArray();

        $this->set([
            'user' => $this->Auth->user(),
            'global' => $global,
            'message' => $message
        ]);
    }

    public function inbox()
    {
        $inbox = TableRegistry::get('Inbox')
            ->find()
            ->contain(['Messages.Users'])
            ->where(['Inbox.user_id' => $this->Auth->user('id')])
            ->orderDesc('Messages.created')
            ->toArray();

        $this->set('inbox', $inbox);
    }

    public function sent()
    {
        $user = TableRegistry::get('Users')->findByIdWithSentMessages($this->Auth->user('id'));
        $this->set('sent', $user->messages);
    }

    private function logMessageAccessSuccess(Message $message)
    {
        Log::info("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] accessed message #{$message->id}", 'messages');
    }

    private function logMessageAccessError(Message $message)
    {
        if ($this->Auth->user()) {
            Log::warning("User #{$this->Auth->user('id')} {$this->Auth->user('email')} [{$this->request->clientIp()}] tried accessing message #{$message->id}", 'messages');
        } else {
            Log::warning("Anonymous user [{$this->request->clientIp()}] tried accessing message #{$message->id}", 'messages');
        }
    }

    public function details($id, $day, $month, $year, $slug)
    {
        $message = TableRegistry::get('Messages')->get($id, [
            'contain' => [
                'Users',
                'Replies' => ['sort' => ['Replies.created' => 'DESC']],
                'Replies.Users'
            ]
        ]);

        if (empty($message)) {
            throw new NotFoundException;
        }

        $this->set([
            'user' => $this->Auth->user(),
            'message' => $message
        ]);

        $this->logMessageAccessSuccess($message);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $message = $this->Messages->get($id);

        if ($message->attachment_path) {
            $file = new File(WWW_ROOT . 'img' . DS . $message->attachment_path);
            $file->delete();
        }
        
        if ($this->Messages->delete($message)) {
            $this->Flash->success('This message has been deleted.');
        } else {
            $this->Flash->error('Failed deleting message. Please try again later.');
        }

        return $this->redirect(['action' => 'home']);
    }

    // API endpoints

    /**
     * Get list of messages and their replies sent by a specific user (HTTP GET)
     * 
     * url: api/users/123/messages.json
     */
    public function index()
    {
        $userId = $this->request->getParam('user_id');

        if (!$userId) {
            throw new BadRequestException('Viewing all messages not supported. Please provide user id.');
        }

        $message = $this->Messages
            ->find()
            ->where(['user_id' => $userId])
            ->contain('Replies');

        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }
}
