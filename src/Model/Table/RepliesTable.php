<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class RepliesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users');
        $this->belongsTo('Messages');
        // https://book.cakephp.org/3.0/en/orm/behaviors/counter-cache.html#basic-usage
        $this->addBehavior('CounterCache', [
            'Messages' => ['reply_count']
        ]);
    }
}
