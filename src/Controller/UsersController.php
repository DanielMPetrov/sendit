<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->action;

        // All registered users can view list of users
        if ($action == 'index') {
            return true;
        }

        return parent::isAuthorized($user);
    }

    public function index()
    {
        $this->set([
            'users' => $this->Users->find(),
            'isAdmin' => $this->Auth->user('role') === 'admin'
        ]);
    }

    public function messages($id) {
        $user = $this->Users->findByIdWithSentMessages($id);
        $this->set('user', $user);
    }
}
