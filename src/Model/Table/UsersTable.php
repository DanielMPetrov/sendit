<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasMany('Messages');
        $this->hasMany('Inbox');
        $this->hasMany('Replies');
    }

    // https://book.cakephp.org/3.0/en/tutorials-and-examples/blog-auth-example/auth.html
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('first_name', 'Name is required')
            ->notEmpty('last_name', 'Surname is required')
            ->notEmpty('email', 'An email is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'user']],
                'message' => 'Please enter a valid role'
            ]);
    }

    public function findByIdWithSentMessages($id)
    {
        return $this->get($id, [
            'contain' => [
                'Messages.inbox.users',
                'Messages' => ['sort' => ['Messages.created' => 'DESC']]
            ]
        ]);
    }
}
