<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Text;

class Message extends Entity
{
    protected function _setSubject($subject)
    {
        $this->slug = strtolower(Text::slug($subject));
        return $subject;
    }

    /**
     * Returns a comma separated list of user emails message was sent to.
     * Requires inbox.users to be loaded with the message.
     */
    protected function _getToDisplayList()
    {
        $to = '';
        foreach ($this->inbox as $inbox) {
            $to .= $inbox->Users['email'] ?? 'all';
            $to .= ', ';
        }

        return rtrim($to, ', ');
    }

    protected function _getPermalink()
    {
        $id = $this->id;
        $date = $this->created->format('d/m/Y');
        $slug = $this->slug;
        return "/$id/message/$date/$slug";
    }

    protected function _getDisplayDateSent()
    {
        return $this->created->format('F j, Y \a\t g:i a');
    }
}
