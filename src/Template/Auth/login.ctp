<?php $this->assign('title', 'Sign in'); ?>

<div class="mx-auto" style="max-width: 480px;">
    <?= $this->Flash->render() ?>
</div>

<div class="card px-5 py-4 my-5 mx-auto" style="max-width: 480px;">
    <?= $this->Form->create() ?>
        <h1 class="display-4 text-center text-muted mb-4" style="font-size:2rem;">Welcome back</h2>
        <p class="text-center text-muted">Sign in with</p>
        <div class="text-center">
            <button id="btn-facebook" class="btn btn-outline-primary" type="button">
                <i class="fab fa-facebook-f mr-1"></i> Facebook
            </button>
        </div>
        <hr class="my-4">
        <p class="text-center text-muted">Or sign in with credentials</p>
        <div class="form-group mb-4">
            <label for="email">Email</label>
            <input name="email" type="email" id="email" class="form-control form-control-lg">
        </div>
        <div class="form-group mb-4">
            <label for="password">Password</label>
            <input name="password" type="password" id="password" class="form-control form-control-lg">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-outline-primary my-2 px-3 py-2">Sign in</button>
        </div>
    <?= $this->Form->end() ?>
</div>

<?php $this->start('script'); ?>
<script>
    const SENDIT_BASE_URL = '<?= $this->Url->build('/') ?>';
    const SENDIT_FACEBOOK_ENDPOINT = '<?= $this->Url->build('/auth/facebook') ?>';
</script>
<?= $this->Html->script('https://www.gstatic.com/firebasejs/5.7.3/firebase.js') ?>
<?= $this->Html->script('https://unpkg.com/axios/dist/axios.min.js') ?>
<?= $this->Html->script('auth') ?>
<?php $this->end(); ?>
